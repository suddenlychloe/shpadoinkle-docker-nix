{
  pkgs             ? import <nixpkgs> {},
  startCmd         ? '''',
  port             ? 80,
  imgName          ? "myImage",
  internalPort     ? 8080,
  extraNginxConfig ? "",
  clientDrv,
  serverDrv
}:
let
  inherit (pkgs)             writeTextFile;
  inherit (pkgs.dockerTools) buildImage shadowSetup;

  writeNginxConfig = name: text: writeTextFile {
    inherit name text;
    checkPhase = ''
      ${pkgs.nginx} -t ${name}
    '';
  };

  nginxConf        = writeNginxConfig "nginx.conf" ''
    proxy_cache_path /var/cache/app levels=1:2 keys_zone=app_cache:10m max_size=10g inactive=60m use_temp_path=off;
    http {
      access_log /dev/stdout;
      server {
        listen ${toString port};
        index index.html;
        location ~* .(jpe?g|svn|png|gif|ico|webmanifest)$ {
          proxy_cache app_cache
          try_files ${clientDrv}/$uri ${clientDrv}/$uri.html ${clientDrv}/$uri/ =404;
        }
        location * {
          proxy_pass localhost:${toString internalPort};
        }
      }
    }
    ${extraNginxConfig}
  '';
in buildImage {
  name          = imgName;
  tag           = "latest";
  contents      = pkgs.nginx;

  extraCommands = ''
    mkdir -p /var/log/nginx
    mkdir -p /var/cache/nginx
    mkdir -p /var/cache/app
  '';
  runAsRoot     = ''
    #!${pkgs.stdenv.shell}
    ${shadowSetup}
    groupadd --system nginx
    useradd --system --gid nginx nginx
    ${serverDrv} --port ${internalPort} &
    ${startCmd}
  '';

  config        = {
    Cmd          = [ "nginx" "-c" nginxConf ];
    ExposedPorts = {
      "${port}/tcp" = {};
    };
  };
}
